#!/usr/bin/python
#  -*-  coding:  iso-8859-1  -*-

"""Test CamCASP using formamide-isa example.
"""

import argparse
import re
import os.path
# import string
import subprocess
from time import sleep
from shutil import rmtree 

parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Test CamCASP using formamide isa-A example.
""",epilog="""
Normally run via the CamCASP tests/run_tests.py script.
To run standalone, use
test_formamide-isa.py --scfcode {dalton|nwchem|psi4} [--dirname <directory>]
The default directory name for the calculation is "test". Specify --done
if the calculation has already been done and you just wish to repeat the
analysis.
""")


# parser.add_argument("", help="Positional argument")
parser.add_argument("--scfcode", default="dalton",
                    choices=["dalton","nwchem","psi4"],
                    help="Ab initio code to use (dalton, nwchem or psi4)")
parser.add_argument("--done", help="CamCASP calculation already complete",
                    action="store_true")
parser.add_argument("--dirname", "-d", default="test",
                    help="Name of directory for test job (default test)")
parser.add_argument("--verbosity", help="Verbosity level", type=int,
                    default=0)
parser.add_argument("--clean", help="Delete files created by previous tests and exit",
                    action="store_true")
parser.add_argument("--debug", help="Keep scratch files for debugging purposes",
                    action="store_true")
parser.add_argument("--difftool", default="xxdiff",
                    help="Difference tool for checking results")
args = parser.parse_args()

camcasp = os.getenv("CAMCASP")
base = os.path.join(camcasp,"tests","formamide-isa")
name = args.dirname

#  Needs to be changed
if args.clean:
  os.chdir(base)
  if os.path.exists(name):
    rmtree(name)
  if os.path.exists("submit.log"):
    os.remove("submit.log")
  if os.path.exists("test_report"):
    os.rename("test_report","previous_test_report")
  exit(0)

done = args.done
os.chdir(os.path.join(base,args.scfcode))

#  Set job running unless already done
if not done:
  with open("submit.log","w") as OUT:
    cmnd = ["runcamcasp.py", "HCONH2", "--clt", "HCONH2-isa.clt",
                     "--directory", name, "--ifexists", "delete",
                     "-M", "2", "--verbosity", str(args.verbosity)]
    if args.debug:
      cmnd.append("--debug")
    rc = subprocess.call(cmnd,
                    stdout=OUT, stderr=subprocess.STDOUT)
  if rc > 0:
    print "Error in job submission -- cancelled"
    exit(4)
  with open("submit.log") as OUT:
    submit_log = OUT.read()
  if (re.match(r'error', submit_log, flags=re.I) or
      os.path.exists(os.path.join(name,"cluster_error"))):
    print submit_log
    print "Error in job submission -- cancelled"
    exit(4)
  else:
    print "Job submitted"
  
#  Monitor progress
ok = True
while not done:
  if args.verbosity > 0:
    print "Waiting"
  sleep(5)
  # print name[task]
  out = os.path.join(base,args.scfcode,name,"OUT")
  #  Wait for output file to appear in the maindir output directory
  if os.path.exists(out):
    if os.path.exists(os.path.join(out,"HCONH2.out")):
      sleep(1)
      with open(os.path.join(out,"HCONH2.log")) as LOG:
        log = LOG.read()
        if re.search("CamCASP finished normally", log):
          print "Calculation finished"
          ok = True
          done = True
        else:
          print "Calculation failed -- see log"
          ok = False
          done = True

if not ok:
  print "Can't continue. Check failed job and rerun"
  exit(3)

try:
  s = subprocess.check_output("cmp check/OUT/formamide-ISA-GRID.mom {}/OUT/formamide-ISA-GRID.mom".format(name), shell=True)
  print "Test successful"
  exit(0)
except subprocess.CalledProcessError:
  if os.path.exists(os.path.join(base,args.scfcode,name,"OUT","formamide-ISA-GRID.mom")):
    print "Test and check results differ."
    diff = args.difftool
    try:
      #  Does the specified (or default) difftool exist?
      typeout = subprocess.check_output("type {}".format(diff),
                                      stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError:
      #  No, it doesn't.
      print "{} not found. Can't display differences.".format(diff)
      diff = "<difftool>"
      print """To display differences, execute
{} {}/{}/{}check,{}{}/OUT/formamide-ISA-GRID.mom""".format(diff,base,args.scfcode,"{",name,"}")
      print "where {} is a difference display program such as xxdiff, meld or vimdiff.".format(diff)
      exit(2)
    #  Yes, it does.
    subprocess.Popen("{d} {b}/{scf}/check/OUT/formamide-ISA-GRID.mom {b}/{scf}/{t}/OUT/formamide-ISA-GRID.mom".format(
      d=diff,b=base,scf=args.scfcode,t=name), shell=True)
else:
  print "Calculation of CO2 multipole moments failed"
  exit(1)

