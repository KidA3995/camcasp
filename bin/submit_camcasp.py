#!/usr/bin/python
#  -*-  coding:  utf-8  -*-

"""Submit a job to run a CamCASP calculation
"""

import argparse
import re
import os
# import string
import subprocess
from camcasp import pbs_header, ge_header

this = __file__
parser = argparse.ArgumentParser(
formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Submit a job to run a CamCASP calculation.
""",epilog="""
{} [--queue <queue>] <runcamcasp.py arguments>

e.g. {} H2O -d avtz-isa

or:
runcamcasp.py --setup H2O -d avtz-isa
and after editing the job files:
{} --restart H2O -d avtz-isa

The default queue is whatever is set in environment variable QUEUE,
or "batch" if it is unset. The --queue option, if present, must precede
all other arguments. The runcamcasp.py job name must be the first of the
remaining (runcamcasp.py) arguments.

If a queue management system is in use, set the environment variable
SCHEDULER to the name of the system, e.g. PBS or GE. The submitted job
requires a header giving information neede by the scheduler. Examples
are at the end of this script; they will probably need to be edited for
your particular system.
""".format(this,this,this))

here = os.getcwd()
scheduler = os.getenv("SCHEDULER")
queue = os.getenv("QUEUE")
# cores = os.getenv("CORES")
if not scheduler and not queue:
  #  Use the Linux batch queue unless overridden
  queue = "batch"

parser.add_argument("job", help="camcasp job name")
parser.add_argument("--queue", help="Job queue to use", default=queue)

# runcamcasp.py arguments
parser.add_argument("arglist", nargs=argparse.REMAINDER,
                    help="Arguments to pass to the submitted runcamcasp.py process")

args = parser.parse_args()
arglist = args.arglist
if args.queue:
  queue = args.queue
  
CamCASP = os.getenv("CAMCASP")
if not CamCASP:
  print("The environment variable CAMCASP must be set to the CamCASP base directory")
  print("Cannot continue")
  exit(1)


#  Find the directory specified in the runcamcasp.py arguments
if arglist.count("-d"):
  ix = arglist.index("-d")
elif arglist.count("--directory"):
  ix = arglist.index("--directory")
else:
  print("No directory name found in command line {}".format(args.cmnd))
  exit(1)
dir = args.arglist[ix+1]

#  Collect the runcamcasp.py arguments into a single string
argstring = ""
while arglist:
  argstring += arglist.pop(0)+" "

# print argstring

def get_header(scheduler):
  if not scheduler:
    #  SCHEDULER unset or null
    return("")
  elif scheduler == "PBS":
    return pbs_header.format(JOB=args.job,QUEUE=queue,NPROC=os.getenv("CORES"))
  elif scheduler == "GE":
    nproc = os.getenv("CORES")
    return ge_header.format(JOB=args.job,NPROC=os.getenv(CORES))

#  Construct the job script
header = get_header(os.getenv("SCHEDULER"))
jobscr = os.path.join(here,dir+".sh")
with open(jobscr,"w") as S:
  S.write(header)
  S.write("""
/bin/bash <<EOF

export PATH={c}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

echo $PATH

cd {h}
{c}/bin/runcamcasp.py {j} {a}

#  Blank line above is needed to give default response to --ifexists query, i.e. save
EOF
""".format(h=here,c=CamCASP, j=args.job, a=argstring))


if queue in ["batch","b"]:
  subprocess.call("batch < {}".format(jobscr), shell=True)
else:
  # print "qsub", "-q", queue, jobscr
  subprocess.call(["qsub", "-q", queue, jobscr])


