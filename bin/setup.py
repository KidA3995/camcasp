#!/usr/bin/python
#  -*-  coding:  iso-8859-1  -*-

"""Script to set up links to CamCASP helper applications.
"""

import argparse
from glob import glob
import os
import re
import readline
import shutil
import subprocess

home = os.environ["HOME"]
camcasp = os.environ.get("CAMCASP")
if not camcasp:
  print "The environment variable CAMCASP must be set to the base CamCASP directory"
  exit(1)
print "{} is {}".format("CAMCASP", camcasp)
PATH = os.environ["PATH"]
if not re.search(camcasp, PATH):
  print "{} should be in your PATH".format(camcasp)
arch = os.environ.get("ARCH")

os.chdir(camcasp)

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
description="""Set up links to CamCASP helper applications.
""",epilog="""
It is not necessary to provide any arguments. The script will request
information as required. File and directory pathnames can include shell
metacharacters.
""")

# parser.add_argument("--base", help="Full path to the CamCASP base directory",
#                     default=camcasp)
# parser.add_argument("--arch", help="Machine architecture", default=link["arch"],
#                     choices=["x86-64","osx"])
# parser.add_argument("--compiler", help="select from available binaries",
#                     default="gfortran", choices=["gfortran","ifort","pgf90"])
# parser.add_argument("--dalton-dir", help="Full path to the Dalton executable",
#                     default=link["dalton"])
# parser.add_argument("--dalton-version", choices=["2.0","2006","2013"],
#                     help="(2.0 and 2006 are the same)")
# parser.add_argument("--nwchem-dir", help="Full path to the NWChem executable",
#                     default=link["nwchem"])
# parser.add_argument("--orient", help="Full path to the base directory for Orient",
#                     default=link["orient"])
#
#  This statement is needed to set up the --help argument. 
args = parser.parse_args()

global archs, compilers
archs = ["x86-64", "osx"]

compilers = {}
compilers["x86-64"] = ["pgf90", "gfortran", "ifort"]
compilers["osx"] = ["gfortran"]
scflist = ["dalton","dalton2006","nwchem","psi4"]
installed = {}

def fullpath(s):
  """Find a full pathname by expanding shell characters."""
  #  glob.glob doesn't recognize tilde
  s = re.sub(r'^~/', home + "/", s)
  ss = glob(s)
  if len(ss) > 1:
    print "Ambiguous:"
    for s in ss:
      print s
    return ""
  elif len(ss) == 0:
    print "{} not found".format(s)
    return ""
  else:
    return ss[0]
  

def base ():
  """Find the base directory."""

  global link, camcasp
  ok = False
  while not ok:
    string = raw_input("\nFull path to the CAMCASP base directory [{}]: ".format(camcasp))
    if string == "" or re.match(r'y', string, flags=re.I):
      pass
    else:
      camcasp = os.path.abspath(string)
    if camcasp == "":
      pass
    elif not os.path.exists(camcasp):
      print "{} doesn't exist".format(camcasp)
    elif not os.path.isdir(camcasp):
      print "{} isn't a directory".format(camcasp)
    elif not os.path.exists(os.path.join(camcasp,"bin","setup.py")):
      print "Sorry, {} can't be right".format(camcasp)
    else:
      ok = True
      os.environ["CAMCASP"] = camcasp
    if not ok:
      string = raw_input("Try again? [Yn] ")
      if re.match(r'n', string, flags=re.I):
        exit(1)
  print "{} -> {}".format("CAMCASP",os.environ["CAMCASP"])
  #  Is $CAMCASP/bin in the PATH?
  paths = os.environ["PATH"].split(":")
  if os.path.join(camcasp,"bin") not in paths:
    print "{} should be in your PATH".format(os.path.join(camcasp,"bin"))

def architecture():
  """Find the machine operating system."""
  arch = os.environ.get("ARCH")
  if arch: arch = arch.lower()
  uname = subprocess.check_output("uname", shell=True).strip()
  if uname == "Linux":
    print "Running under Linux"
    if arch == "x86-64":
      pass
    else:
      print "Please set the environment variable ARCH to 'x86-64'"
      arch = "x86-64"
  elif uname == "Darwin":
    print "Running under Darwin (OSX)"
    if arch == "osx":
      pass
    else:
      print "Please set the environment variable ARCH to 'osx'"
      arch = "osx"
  else:
    print "Running under {} -- not supported, sorry.".format(uname)
    exit(1)
  return arch

def binaries(arch):
  """Find the binaries. Not needed for public branches."""

  global compilers
  compiler = ""
  ok = False
  while not ok:
    string = raw_input("\nFind binaries from compiler (RETURN to skip this step) [{}]: ".format(compiler))
    if string == "":
      print "Skipped"
      return
    elif re.match(r'y', string, flags=re.I):
      ok = True
    else:
      compiler = string.lower()
      if compiler not in compilers[arch]:
        print "Supported compilers for {} are {}".format(arch,compilers[arch])
        compiler = ""
      else:
        ok = True
  root = os.path.join(os.environ["CAMCASP"],arch,compiler)
  print "Looking for binaries from compiler {}".format(compiler)
  allok = True
  for program in ["camcasp","cluster","process","casimir","pfit"]:
    ok = True
    if os.path.islink(os.path.join("bin",program)):
      os.remove(os.path.join("bin",program))
    path = os.path.join(root,program)
    if not os.path.exists(path):
      #  Look in root/exe/program (root/exe/pfit/pfit for pfit)
      if program == "pfit":
        path = os.path.join(root,"exe/pfit/pfit")
      else:
        path = os.path.join(root,"exe",program)
      if not os.path.exists(path):
        #  Look in root/program
        path = os.path.join(root,program)
        if not os.path.exists(path):
          print "Can't find a {} binary for architecture {} and compiler {}".format(program,arch,compiler)
          ok = False
          allok = False
    if ok:
      os.symlink(path,os.path.join("bin",program))
      print "bin/{} -> {}".format(program,path)
  if allok: print "All found"


def verify():
  """Verify execution arrangements for scfcodes"""
  installed = {}
  for name in ["dalton","dalton2006","nwchem","psi4","orient","sapt"]:
    q = raw_input("\nIs the {} program installed [yN]? ".format(name))
    if q in ["", "N", "n"]:
      installed[name] = False
      continue
    installed[name] = True
    if os.path.exists(os.path.join(camcasp,"bin","{}.sh".format(name))):
      print os.path.join(camcasp,"bin","{}.sh".format(name)), \
        "will be invoked to execute {}.".format(name)
      print "Please check that it is set up correctly for your system."
    else:
      try:
        typeout = subprocess.check_output("type {}".format(name),
                                          stderr=subprocess.STDOUT, shell=True)
      except subprocess.CalledProcessError:
        print "{} not found.".format(name)
        print "Ensure that the directory containing the program executable is in your PATH"
        print "or make a symbolic link to the executable in the CAMCASP/bin directory."
      else:
        print typeout.strip()
        s = typeout.split()[-1]
        filetype = subprocess.check_output("file {}".format(s),
                                          stderr=subprocess.STDOUT, shell=True)
        print filetype.strip()
        installed[name] = True
        
    #  Special checks
    if name == "dalton" and installed[name]:
        if os.path.islink("bin/readDALTONmos"):
          os.remove("bin/readDALTONmos")
        print """
Dalton can be compiled with the --int64 option to use 64-bit integers.
However the default and recommendation is to use 32-bit integers."""
        string = raw_input("Was Dalton compiled with 64-bit integers? [yN] ")
        if string in ["n", "N", ""]:
          os.symlink("readDALTON32mos","bin/readDALTONmos")
        else:
          os.symlink("readDALTON64mos","bin/readDALTONmos")

    elif name == "dalton2006" and installed[name]:  
        if os.path.islink("bin/readDALTON2006mos"):
          os.remove("bin/readDALTON2006mos")
          os.symlink("readDALTON32mos","bin/readDALTON2006mos")

    elif name == "psi4" and installed[name]:
        #  Check environment variables
        print "\nChecking psi4 environment variables:"
        if "PSI4_HOME" in os.environ:
          if os.path.isdir(os.environ["PSI4_HOME"]):
            print "PSI4_HOME = {}".format(os.environ["PSI4_HOME"])
          else:
            print "PSI4_HOME appears to be set incorrectly"
        else:
          print "PSI4_HOME is unset"
        pathok = False
        if "PSIPATH" in os.environ:
          paths = os.environ["PSIPATH"].split(":")
          if os.path.join(camcasp,"basis","psi4") in paths:
            pathok = True
        if pathok:
          print "PSIPATH includes {}: ok".format(os.path.join(camcasp,"basis","psi4"))
        else:
          print "The environment variable PSIPATH must be set to include \n{}".format(os.path.join(camcasp,"basis","psi4"))
        if "PSI_SCRATCH" in os.environ and os.path.isdir(os.environ["PSI_SCRATCH"]):
          print "PSI_SCRATCH = {}".format(os.environ["PSI_SCRATCH"])
        else:
          print "The environment variable PSI_SCRATCH must be set to a suitable scratch directory"
  present = [installed[g] for g in ["dalton","dalton2006","nwchem","psi4"]]
  if any(present):
    pass
  else:
    print """
IMPORTANT
You need to have at least one of the following SCF packages installed:
  Dalton 2013 or later
  Dalton 2006 (Dalton 2.0)
  NWChem
  Psi4
"""
  if not installed["orient"]:
    print """The Orient program is needed for some procedures, in particular to
localize polarizabilities and obtain dispersion coefficients."""


base()
os.chdir(camcasp)
print "Setting up links for {}".format(camcasp)

arch = architecture()
# binaries(arch)
verify()
