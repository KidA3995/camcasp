#!/usr/bin/python
# -*- coding: latin-1 -*-

"""Extract sapt-dft energy terms from CamCASP summary files.
"""

import os
import re
import string
import sys
import argparse
from camcasp import die, findfile

parser=argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter,
description="""Extract sapt-dft energy terms from CamCASP summary files.
""",epilog="""

There may be any number of job directory paths in the argument list.
Path arguments that are not directories are silently ignored.
The script looks for CamCASP summary files, which are expected to have
names of the form <path>/OUT/<job><suffix>. The default suffix is
"-data-summary.data", which is the usual form used by CamCASP. The job
name does not have to be the same for every path.

If a directory <path>_dHF is present in the argument list, it is assumed
to contain a delta-HF calculation, and the delta-HF energy is extracted
from it. If there are directories <path> and <path>_dHF with the same
<path>, they are assumed to refer to the same system and the energy
components are displayed together. However the script will handle cases
where only the SAPT-DFT calculation or only the delta-HF calculation has
been done.

Energy values in the input file are converted to the specified unit,
default kJ/mol.

If --ab is specified, the induction terms for molecules A and B are shown
separately. In the short layout, the ind, exind and delta-HF components are
printed as a total ind term, while disp and exdisp are printed as a total
disp term. In the long layout they are all printed separately.

If --reg is specified, the regularised second-order induction and exchange-induction
energies will be used to calculate the total interaction energy. The default
regularization parameter is eta = 3.0 a.u. This can be changed using
  --regeta  <value>

If --ct is specified, the second-order charge-transfer, CT(2), and 
polarization energy, POL(2), will be calculated using the regularized
induction and exchange-induction energies. As above, the default
regularization parameter is eta = 3.0 a.u., but this can be changed
using --regeta.

If the optional --split flag is used, it should provide a re.match pattern
for the directory names, and a list of matched groups that are to be output
as data columns. E.g.
extract_saptdft.py  H2O-HOH_*_dc{,_dHF} --split 'H2O-HOH_(.....)_(...)_dc' 1 2
where the actual directory names are of the form, e.g., H2O-HOH_1.957_135_dc
Note that the pattern will be substituted by the shell.
"""
)
# parser.add_argument("--job", help="job file prefix", default="")
parser.add_argument("paths", metavar="path", nargs="+",
                    help="path to job directory (may be repeated)")
parser.add_argument("--verbose", "-v", help="verbose output",
                    action="count")
parser.add_argument("--unit", "--units", help="Unit for output (default kJ/mol)",
                    choices=["cm-1","kJ/mol","au","hartree","eV","meV","K","kelvin","kcal/mol"],
                    default="kJ/mol")
parser.add_argument("--reg", help="Use regularized induction and exchange-induction",
                    action="store_true")
parser.add_argument("--ct", "--CT", help="Compute the CT(2) energy using regularized induction",
                    action="store_true")
parser.add_argument("--regeta", help="Value of regularization parameter \eta (normally 3.0)",
                    default=3.0, type=float)
layout_group = parser.add_mutually_exclusive_group()
layout_group.add_argument("--short", help="Short output layout",
                    action="store_true")
layout_group.add_argument("--long", help="Long output layout",
                    action="store_true")
parser.add_argument("--S2", help="Use S2 approximation for E(1)exch and E(2)exind",
                    action="store_true")
parser.add_argument("--ab", help="Show A and B induction separately",
                    action="store_true")
parser.add_argument("--suffix", help="summary file suffix (default -data-summary.data)",
                    default="-data-summary.data")
parser.add_argument("--dhf", help="default delta-HF energy",
                    action="store_true")
parser.add_argument("--split", nargs="*",
                    help="Split directory name to provide variable data")
parser.add_argument("--title", help="Optional title for output")

args = parser.parse_args()
# job = args.job

ct_reg     = args.ct
regularize = args.reg
# print "ct_reg and regularize ",ct_reg, regularize
reg_eta    = float(args.regeta)
if regularize or ct_reg:
    if reg_eta==0.0:
        reg_eta = 3.0 # this is the default value for E(2)IND(reg)

# Define terms in Eint:
if regularize:
  saptdft = ["elst","exch","indRA","indRB","exindRA","exindRB","disp","exdisp"]
else:
  saptdft = ["elst","exch","indA","indB","exindA","exindB","disp","exdisp"]

show_dhf = False
if args.dhf: # (and later if any _dHF directories are encountered)
  saptdft.append(["dHF"])
  show_dhf = True

if args.split:
  print "directory name pattern:", args.split


units = {
"kj/mol": 2625.5,
"cm-1":   219475.0,
"au":          1.0,
"hartree":     1.0,
"ev":      27.2113,
"mev":     27211.3,
"k":       315773.0,
"kelvin":  315773.0,
"kcal/mol":627.510,
}
unit = units[string.lower(args.unit)]
# print unit 

name_len = 0
energy = {}
stderr = sys.stderr

if args.title:
  print args.title

for path in args.paths:
  path = os.path.normpath(path)
  if not os.path.isdir(path):
    continue
  if not os.path.exists(os.path.join(path,"OUT")):
    continue
  if re.search(r'_dHF$', path):
    name = re.sub(r'_dHF$', '', path)
    isdhf = True
    if not show_dhf:
      saptdft.append("dHF")
    show_dhf = True
  else:
    name = path
    isdhf = False
  summary = findfile(os.path.join(path,"OUT"),args.suffix)
  if not summary:
    continue
  job = re.sub(args.suffix, "", os.path.basename(summary))

  if args.verbose > 0: print path, name
  if name not in energy:
    energy[name] = {}
    name_len = max(name_len,len(name))

  if isdhf:
    #  Delta-HF
    ok = True
    for suffix in ["A","B","AB"]:
      file = os.path.join(path, "OUT", job + "_" + suffix + ".out")
      if not os.path.exists(file):
        ok = False
        if args.verbose > 0: stderr.write("No file " + file + "\n")
        break
    if not ok:
      continue
    ehf = {}
    for suffix in ["A","B","AB"]:
      ok = False
      file = os.path.join(name + "_dHF", "OUT", job + "_" + suffix + ".out")
      if not os.path.exists(file):
        print "Can't find file", file
        exit(1)
      if args.verbose > 0:
        print file
      with open(file) as IN:
        for line in IN:
          m = re.match(r'\@? +(Final HF energy:|Total SCF energy =|Total Energy =) +(-?\d+\.\d+)',line)
          if m:
            ehf[suffix] = float(m.group(2))
            ok = True
            break
        if not ok:
          die("Can't find HF energy in " + file)
    ehf_diff = (ehf["AB"] - ehf["A"] - ehf["B"])*unit
    if args.verbose > 1:
      print "E_AB =", ehf["AB"]*unit, "E_A =", ehf["A"]*unit, "E_B =", ehf["B"]*unit
  
    with open(summary) as IN:
      for line in IN:
        #  Ignore regularized energy terms
        if re.search(r' REG ', line):
          continue
                                                          # Groups
        energies = re.compile(r'''E\^\{[12]\}             #     E^{n} where n = 1 or 2 
                                  \_\{(\w+)(\,exch)?\}    # 1 2 _{component} or _{component,exch}
                                  (\(S2\))?               # 3   match (S2) if present
                                  (\((A|B)\))?            # 4 5 match (A) or (B) if present
                                  \s+                     #     space. group(5) will be A or B.
                                  (-?\d+\.\d+(E[+-]\d+)?) # 6 7 match a floating point number of the
                                                          #     form (-)mmmm.nnnnE(+|-)pp
                                                          #     The exponential is optional and
                                                          #     will be group(7)
                                  \s+                     #     space
                                  (\S+)                   # 8   text
                                  \s+                     #     more space
                                  ([\S,\s]*)              # 9   text with space
                                  ''',re.I|re.VERBOSE)
        m = energies.search(line)

        if m:
          # print 'dHF GOT LINE ',line
          # print 'Groups  1: ',m.group(1),' 2: ',m.group(2),' 3: ',m.group(3)
          # print 'Groups  4: ',m.group(4),' 5: ',m.group(5),' 6: ',m.group(6)
          # print 'Groups  7: ',m.group(7),' 8: ',m.group(8),' 9: ',m.group(9)
          cmpnt = m.group(1)
          # Special case to decide whether or not to use S2 approx for exchange energy:
          if cmpnt == "exch":
            if (m.group(3) == "(S2)" and not args.S2) or (m.group(3) != "(S2)" and args.S2):
              continue
          if m.group(2) == ",exch":
            cmpnt = "ex" + cmpnt
          # Another special case to decide whether or not to use S2 approx for exch-ind energy:
          if cmpnt == "exind":
            if (m.group(3) == "(S2)" and not args.S2) or (m.group(3) != "(S2)" and args.S2):
              continue
          if m.group(5) != "":
            if m.group(5) in ["A","B"]:
              cmpnt = cmpnt + m.group(5)
          value = float(m.group(6))
          inunit = m.group(8)
          if args.verbose > 0: print "{:6s} {:10.3f} {:3s}".format(cmpnt, value, inunit)
          u = units[string.lower(inunit)]
          value = value*unit/u
          ehf[cmpnt] = value
    #  Old output files may not have the full E^(1)_exch as well as E^(1)_exch(S2)
    if "exch" in ehf:
      energy[name]["dHF"] = ehf_diff \
          - ehf["elst"] - ehf["exch"] - ehf["ind"] -ehf["exind"]
    else:
      if args.S2:
        print "No exch(S2) value in", path
      else:
        print "No full exch value in", path

  else:
    #  Normal sapt-dft
    with open(summary) as IN:
      for line in IN:
                                                          # Groups
        energies = re.compile(r'''E\^\{[12]\}             #     E^{n} where n = 1 or 2 
                                  \_\{(\w+)(\,exch)?\}    # 1 2 _{component} or _{component,exch}
                                  (\(S2\))?               # 3   match (S2) if present
                                  (\((A|B)\))?            # 4 5 match (A) or (B) if present
                                  \s+                     #     space. group(5) will be A or B.
                                  (-?\d+\.\d+(E[+-]\d+)?) # 6 7 match a floating point number of the
                                                          #     form (-)mmmm.nnnnE(+|-)pp
                                                          #     The exponential is optional and
                                                          #     will be group(7)
                                  \s+                     #     space
                                  (\S+)                   # 8   text
                                  \s+                     #     more space
                                  ([\S,\s]*)              # 9   text with space
                                  ''',re.I|re.VERBOSE)
        m = energies.search(line)
        if m:
          # print 'GOT LINE ',line
          # print 'Groups  1: ',m.group(1),' 2: ',m.group(2),' 3: ',m.group(3)
          # print 'Groups  4: ',m.group(4),' 5: ',m.group(5),' 6: ',m.group(6)
          # print 'Groups  7: ',m.group(7),' 8: ',m.group(8),' 9: ',m.group(9)
          cmpnt = m.group(1)
          # Special case to decide whether or not to use S2 approx for exchange energy:
          if cmpnt == "exch":
            if (m.group(3) == "(S2)" and not args.S2) or (m.group(3) != "(S2)" and args.S2):
              continue
          if m.group(2) == ",exch":
            cmpnt = "ex" + cmpnt
          # Another special case to decide whether or not to use S2 approx for exch-ind energy:
          if cmpnt == "exind":
            if (m.group(3) == "(S2)" and not args.S2) or (m.group(3) != "(S2)" and args.S2):
              continue
          #Special case for ind and exind: 
          if cmpnt == "ind" or cmpnt == "exind":
            # Search group(10) for the string: REG eta = <value>
            if re.search(r' REG ', line):
              reg = re.compile(r'''
                  REG\s+eta\s+=\s+         #    REG eta = 
                  (-?\d+\.\d+(E[+-]\d+)?)  # 1  value
                                ''',re.I|re.VERBOSE)
              mm = reg.search(m.group(9))
              if mm:
                  if mm.group(1):
                      eta = float(mm.group(1))
                      if reg_eta == eta:
                          cmpnt = cmpnt + "R"
                      else:
                          continue
                  else:
                      continue
              else:
                  continue
          if m.group(5) != "":
            if m.group(5) in ["A","B"]:
              cmpnt = cmpnt + m.group(5)
          value = float(m.group(6))
          inunit = m.group(8)
          if args.verbose > 0: print "{:6s} {:10.3f} {:3s}".format(cmpnt, value, inunit)
          u = units[string.lower(inunit)]
          value = value*unit/u
          energy[name][cmpnt] = float(value)
    if args.dhf and "dHF" not in energy[name]:
      energy[name]["dHF"] = float(args.dhf)
    if "exch" not in energy[name]:
      if args.S2:
        print "No exch(S2) value in", path
      else:
        print "No full exch value in", path


if args.short:
  exind = []
  disp = ["DISP"]
  if regularize:
    if args.ab:
      ind = ["INDRA", "INDRB"]
    else:
      ind = ["INDR"]
  else:
    if args.ab:
      ind = ["INDA", "INDB"]
    else:
      ind = ["IND"]
else:
  disp = ["disp","exdisp"]
  if regularize:
    if args.ab:
      ind = ["indRA","exindRA","indRB","exindRB"]
    else:
      ind = ["indR","exindR"]
  else:
    if args.ab:
      ind = ["indA","exindA","indB","exindB"]
    else:
      ind = ["ind","exind"]

display = ["elst","exch"] + ind
if show_dhf:
  display.append("dHF")
display += disp + ["Eint"]

if ct_reg:
  if args.ab:
    display_extra = ["ctA","ctB","CT","polA","polB","POL"]
  else:
    display_extra = ["CT","POL"]
else:
  display_extra = []

#  Output header
buffer = "{:2s} ".format(args.unit) + " " * (max(name_len-len(args.unit)+4,0))
for s in display:
  if args.S2:
    if s == "exch":
      s = "exch(S2)"
    if s == "exind":
      s = "exind(S2)"
    if args.short:
      if s == "IND":
        s = "IND(S2)"
      if s == "INDA":
        s = "INDA(S2)"
      if s == "INDB":
        s = "INDB(S2)"
  buffer += "{:^9s}   ".format(s)

for s in display_extra:
  buffer += "{:^9s}   ".format(s)

print buffer.rstrip()

#  Output for each job (directories <d> and <d>_dHF together under <d>)
for name in sorted(energy.keys()):
  buffer = ""
  if args.split:
    # print args.split
    m = re.match(args.split[0], name)
    if m:
      buffer = ""
      for i in range(1,len(args.split)):
        p = m.group(int(args.split[i]))
        buffer += "  {}".format(p)
  else:
    buffer = name
  buffer += " " * (name_len-len(buffer))
  if args.short:
    energy[name]["IND"]  = energy[name]["ind"]  + energy[name]["exind"]
    energy[name]["INDA"] = energy[name]["indA"] + energy[name]["exindA"]
    energy[name]["INDB"] = energy[name]["indB"] + energy[name]["exindB"]
    energy[name]["DISP"] = energy[name]["disp"] + energy[name]["exdisp"]
    if regularize:
      energy[name]["INDR"]  = energy[name]["indR"]  + energy[name]["exindR"]
      energy[name]["INDRA"] = energy[name]["indRA"] + energy[name]["exindRA"]
      energy[name]["INDRB"] = energy[name]["indRB"] + energy[name]["exindRB"]
    #if not args.ab and "dHF" in energy[name]:
    #  energy[name]["IND"] += energy[name]["dHF"]
  if ct_reg:
    if "indRA" in energy[name] and "exindRA" in energy[name]:
      energy[name]["polA"] = energy[name]["indRA"] + energy[name]["exindRA"]
      energy[name]["ctA"]  = energy[name]["indA"]  + energy[name]["exindA"] - energy[name]["polA"]
    else:
      energy[name]["polA"] = 0.0
      energy[name]["ctA"]  = 0.0
    if "indRB" in energy[name] and "exindRB" in energy[name]:
      energy[name]["polB"] = energy[name]["indRB"] + energy[name]["exindRB"]
      energy[name]["ctB"]  = energy[name]["indB"]  + energy[name]["exindB"] - energy[name]["polB"]
    else:
      energy[name]["polB"] = 0.0
      energy[name]["ctB"]  = 0.0
    energy[name]["POL"]  = energy[name]["polA"]  + energy[name]["polB"]
    energy[name]["CT"]   = energy[name]["ctA"]   + energy[name]["ctB"]
  # Define the SAPT(DFT) interaction energy:
  Eint = 0.0
  for cmpnt in saptdft:
    if cmpnt in energy[name]:
      Eint += energy[name][cmpnt]
  for cmpnt in display:
    if cmpnt == "Eint":
      buffer += " {:11.5f}".format(Eint)
    else:
      if cmpnt in energy[name]:
        if args.verbose > 0: print cmpnt, energy[name][cmpnt]
        buffer += " {:11.5f}".format(energy[name][cmpnt])
      else:
        buffer += "       ---- "
  if ct_reg:
    for cmpnt in display_extra:
      if cmpnt in energy[name]:
        if args.verbose > 0: print cmpnt, energy[name][cmpnt]
        buffer += " {:11.5f}".format(energy[name][cmpnt])
      else:
        buffer += "       ---- "

  print buffer

