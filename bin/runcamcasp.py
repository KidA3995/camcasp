#!/usr/bin/python
#  -*-  coding:  iso-8859-1  -*-


summary = """
Set up and carry out a CamCASP calculation.

CamCASP and related jobs can be set up using this script. A "cluster file"
must be provided, containing the molecular geometries and details of the
calculation to be carried out. See the users' guide for details. Available
calculation types, specified in the cluster file, are:
  properties: multipole moments, polarizabilities, etc. of single molecules.
  sapt-dft: symmetry-adapted perturbation theory of dimer interaction
    energies, using molecular wavefunctions obtained by density functional
    theory.
  sapt: symmetry-adapted perturbation theory using Hartree-Fock wavefunctions
    and perturbative correlation corrections.
  delta-hf: calculation of the delta-HF estimate of higher-order induction
    energy terms.
  supermolecule: standard supermolecule calculation of dimer energies, with
    counterpoise correction for BSSE.
Use "runcamcasp.py --help" for details of arguments.
"""

import os
import sys
import re
import argparse
import readline
import shutil
import string
import subprocess
from time import sleep
from camcasp import *

# queue=os.environ["QUEUE"]
scratch=os.getenv("SCRATCH")

parser=argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter,
description = "Set up and run a CamCASP job.",
epilog = """
CamCASP and related jobs can be set up using this script. A "cluster file"
must be provided, containing the molecular geometries and details of the
calculation to be carried out. See the users' guide for details. Available
calculation types are:
  properties: multipole moments, polarizabilities, etc. of single molecules.
  sapt-dft: symmetry-adapted perturbation theory of dimer interaction
    energies, using molecular wavefunctions obtained by density functional
    theory.
  sapt: symmetry-adapted perturbation theory using Hartree-Fock wavefunctions
    and perturbative correlation corrections.
  delta-hf: calculation of the delta-HF estimate of higher-order induction
    energy terms.
  supermolecule: standard supermolecule calculation of dimer energies, with
    counterpoise correction for BSSE.

The runcamcasp.py script has several options, but in its simplest form it
can be just 
  runcamcasp.py <job> [-d <directory>]
where job is a short name for the job, also used as a prefix for many of the
generated files, and the job files are placed in the specified directory.
Even the directory specification can be omitted (as indicated by the square
brackets, which are not to be typed in). If it is omitted, a directory called
<job> is used. However it is recommended that a new directory is used for
every job. 

So the files for the calculation reside in the directory specified by the
-d or --directory flag. For a restart, the directory must exist already.
Otherwise, if the directory exists, the action is specified by the
--ifexists flag:
  delete  Delete the existing directory and make a new one.
  abort   Leave the directory alone and cancel the new job.
  new, keep, save: Rename the existing directory and make a new one with the
          specified name.
  ask:    Ask what to do. This is the default action.
If the --ifexists flag is specified for a restart, it applies to the
OUT results subdirectory. In this case, new, keep, or save causes the
existing OUT subdirectory, if any, to be renamed and a new OUT
subdirectory created. Otherwise an existing OUT directory is re-used,
and files in it are overwritten.

Note that in a restarted job, this script reads the copy of the .clt
file in the job directory -- not the one in the current directory,
which may have been changed. However none of the files for the job are
regenerated. In particular, the .cks file containing the data for the
CamCASP step is not changed by this script on a restart, so it may be
edited before the restart to change options or correct errors. The
--setup-only flag can be used to stop after setting up the files, to
allow changes to the files before starting the job with the --restart flag.

The SCF code used to obtain the molecular orbitals for the system may be
specified in several ways. In order of priority, from highest to lowest,
1. --scfcode flag on the runcamcasp.py command line, if present.
2. SCFCODE entry in the cluster file, if present.
3. Environment variable CAMCASP_SCFCODE, if set.
4. "dalton", i.e. Dalton-2013 or later.
""")

parser.add_argument("job", help="Job name and prefix for job file names")
parser.add_argument("--clt", help="Name of cluster file (default <job>.clt)")
parser.add_argument("--directory","-d",  help="Directory to run job in (default <job>)")
parser.add_argument("--scfcode", help="Specify scfcode",
                    choices=["dalton2006","dalton","dalton2013","dalton2015",
                             "dalton2016","nwchem","psi4"])
group = parser.add_mutually_exclusive_group()
group.add_argument("--verbosity", type=int, default=0,
                    help="Print additional information about the job if > 0")
group.add_argument("--verbose", "-v", action="count",
                   help="Print additional information about the job")
parser.add_argument("--ifexists", help="Action if directory exists",
                    default="ask", choices=["ask", "delete", "new", "keep", "save", "abort"])
# group.add_argument("--delete", help="delete existing directory unconditionally",
#                     action="store_true")
# group.add_argument("--new", help="keep existing directory and make a new one",
#                     action="store_true")
# group.add_argument("--abort", help="keep existing directory and abort job",
#                     action="store_true")
# group.add_argument("--ask", help="ask whether to delete existing directory or make a new one (default)",
#                     action="store_true")
parser.add_argument("--import", dest="imported", nargs="*", default=[],
                    help="Copy specified files into job directory")
parser.add_argument("-M", "--memory", help="Memory for job in GB",
                    type=int, default=0)
# parser.add_argument("-q", "--queue", help="Queue for job (bg, batch, none)",
#                     default=queue)
parser.add_argument("--scratch", help="Scratch directory (default is the environment variable SCRATCH, if set)",
                    default=scratch)
parser.add_argument("--work", help="Work subdirectory (under scratch)",
                    default="")
parser.add_argument("--cores", help="Number of cores available for all jobs",
                    type=int, default=0)
parser.add_argument("--direct", help="Use direct integral management",
                    action="store_true")
parser.add_argument("--restart", help="Restart job using existing directory",
                    action="store_true")
parser.add_argument("--debug", help="Don't delete scratch files",
                    action="store_true")
parser.add_argument("--setup", "--setup-only", help="Set up files for the job and stop",
                    action="store_true")
parser.add_argument("--log", help="Path to logfile (default OUT/jobname.log)")

args = parser.parse_args()

#  args.job is a required argument. Open an instance of class Job.
job = Job(args.job)
#  Note: job.name = args.job
#  Directory to run job in
if args.directory:
  job.dir = args.directory
else:
  job.dir = args.job

if args.log:
  job.logfile = args.log
else:
  #  Default set in execute function
  job.logfile = ""

# job.queue = args.queue
if args.work:
  job.work = os.path.join(args.scratch,args.work)
else:
  job.work = os.path.join(args.scratch,job.name)
  
job.debug = args.debug

#  Cluster fle
if args.clt:
  job.cltfile = args.clt
else:
  job.cltfile = job.name + ".clt"
job.restart = args.restart
if job.restart:
  job.cltfile = os.path.join(job.dir,job.cltfile)
if not os.path.exists(job.cltfile):
    die("Cluster file {} not found".format(job.cltfile))

#  Has the SCF code been set on the command line?
if args.scfcode in ["dalton","dalton2013","dalton2015","dalton2016"]:
  job.scfcode = "dalton"
else:
  job.scfcode = args.scfcode
#  This will give None if it wasn't set, and it will be set later

#  Verbosity
if args.verbosity:
  verbosity = args.verbosity
elif args.verbose:
  verbosity = args.verbose
else:
  verbosity = 0

# Mark these parameters as unset
job.direct = False
job.memory = 0
# job.wait = 0.0
job.pause = 0.0
job.cores = 0
np_psi4 = 1
np_nwchem = 1

#  Various options that may be set in a .camcasprc file
#  Look for a .camcasprc file in the current directory, or else in the
#  user's home directory or the $CAMCASP directory.
camcasprc = ".camcasprc"
if not os.path.exists(camcasprc):
  #  Look in the users home directory
  camcasprc = os.path.join(os.environ["HOME"],".camcasprc")
elif not os.path.exists(camcasprc):
  #  Look in the CamCASP directory
  camcasprc = os.path.join(os.environ["CAMCASP"],".camcasprc")
if os.path.exists(camcasprc):
  if verbosity > 0:
    print "Reading parameters from {}".format(camcasprc)
  with open(camcasprc) as RC:
    for line in RC:
      if re.match(r' *#', line) or re.match(r' *$', line):
        continue
      if re.match(r' *\w+', line):
        item = line.split()
        word = item[0].lower()
        if word == "memory":
          #  Maximum memory in GB
          job.memory = int(item[1])
        elif word == "direct":
          if item[1].lower() in ["yes", "on", "true"]:
            job.direct = True
          elif item[1].lower() in ["no", "off", "false"]:
            job.direct = False
        elif word == "nproc":
          #  Number of processors available for SCFcodes generally
          nproc = int(item[1])
          np_nwchem = nproc
          np_psi4 = nproc
        elif word == "np_nwchem":
          #  Number of processors available for NWChem
          np_nwchem = int(item[1])
        elif word == "np_psi4":
          #  Number of processors available for Psi4
          np_psi4 = int(item[1])
#         elif word == "queue":
#           job.queue = item[1].lower()

#  These values can be over-ridden on the command line:
if args.direct: job.direct= True
if args.memory: job.memory = args.memory
if args.cores: job.cores = args.cores

#  Default values if still unset
if job.memory == 0:
  # Set memory in GB
  job.memory = 8


job.imports = args.imported

#  Obtain job information from the cluster file
read_clt(job,verbosity)

if job.cores == 0:
  if job.scfcode == "nwchem":
    job.cores = np_nwchem
  elif job.scfcode == "psi4":
    job.cores = np_psi4
  else:
    if os.environ.get("CORES"):
      job.cores = int(os.environ.get("CORES"))
    else:
      job.cores = nproc

if verbosity > 0:
  print "job name:        ", job.name
  print "cluster file     ", job.cltfile
  print "directory:       ", job.dir
  print "if directory exists: ", args.ifexists
  print "memory in GB:    ", job.memory
  # print "queue:           ", job.queue
  print "number of cores available to use:", job.cores
  print "direct integrals:", job.direct
  # print "wait time before starting processes: ", job.wait
  print "restart job:     ", job.restart
  #  Imported files
  if len(job.imports) > 0:
    print "Imported files:"
    for file in job.imports:
      print file


#  Find a directory to run the job in
d = job.dir
#  Remove any trailing slashes from directory name
d = d.rstrip("/")
if args.restart:
  #  In this case the directory must exist.
  if os.path.exists(d):
    job.dir = os.path.abspath(d)
  else:
    die("Directory {} not found".format(d))
  try:
    os.chdir(d)
  except IOError as e:
    if e.errno == errno.EACCES:
      die("Can't open " + d)
    else:
      die("Error for " + d)
  if os.path.exists("OUT"):
    if args.ifexists in ["new","keep","save"]:
      newdir("OUT")
    elif args.ifexists == "delete":
      shutil.rmtree("OUT")
      print "Existing OUT subdirectory deleted"
    elif args.ifexists == "abort":
      print "Subdirectory OUT exists; aborting job"
      exit(2)

else:
  if os.path.exists(d):
    #  Delete or rename existing directory according to --ifexists
    #  argument or interactive response
    if args.ifexists == "delete":
      shutil.rmtree(d)
      print "Directory {} exists; now deleted".format(d)
    elif args.ifexists == "abort":
      print "Directory {} exists; aborting job".format(d)
      exit(2)
    elif args.ifexists in ["new","keep","save"]:
      print "Directory {} exists; saving it and creating another".format(d)
    else:
      print "Directory {} already exists.".format(d)
      ans = raw_input("Delete it and its contents, Save it and make a new one, or Abort job? [dSa]: ")
      # print ans
      if ans in ["D","d"]:
        shutil.rmtree(d)
      elif ans in ["S","s",""]:
        # print "Not deleted."
        pass
      elif ans in ["A","a"]:
        exit(0)
        
  #  At this point, if the specified directory d still exists, it will be
  #  renamed as d_nnn, where d_nnn is the first directory of the form
  #  d_001, d_002, etc. beyond any that already exist. Then a new
  #  directory with the specified name is opened for the new job.
  newdir(d)

  #  Copy files specified by --import into the job directory
  for file in job.imports:
    try:
      shutil.copy2(file, d)
    except IOError:
      die("Can't import file {} into {}".format(file,d))

  #  Copy the cluster file into the job directory, chdir to it
  #  and run the cluster program
  print "Setting up files in directory", d
  shutil.copy(job.cltfile, d)
  job.dir = os.path.abspath(d)
  os.chdir(d)
  print "See {}/{}.clout for output of CLUSTER".format(job.dir,job.cltfile)
  rc = subprocess.call("cluster --scfcode {} --job {} < {} > {}.clout".format(
    job.scfcode,job.name,job.cltfile,job.cltfile),shell=True)
  if rc > 0:
    print "Error {:1d} from cluster -- job aborted".format(rc)
    exit(1)

  if job.scfcode in ["dalton", "dalton2006"]:
    make_dalton_datafiles(job,verbosity)
  elif job.scfcode == "psi4":
    if os.path.exists(job.name+".psi4"):
      #  This file needs to be processed into a jobname_AB.in file
      make_psi4_datafile(job, verbosity)


jobtype = {
  "saptdft": "SAPT(DFT)",
  "deltahf": "Delta-HF",
  "sapt": "SAPT",
  "properties": "properties",
  "supermol": "supermolecule",
  "psi4-saptdft": "Psi4-SAPT(DFT)",
}

if args.setup:
  print """Job files set up.
Use
  runcamcasp.py {j} --clt {c} -d {d} [options] --restart [&]".format(j=job.name,c=job.cltfile,d=d)
or
  submit_camcasp.py [--queue <queue>]  {j} --clt {c} -d {d} [options] --restart
(--queue option first if present) to execute the job.""".format(j=job.name,c=job.cltfile,d=d)
  exit(0)
elif args.restart:
  print "Restarting job in directory", d
else:
  print "This is a {} calculation with SCF program {}".format(jobtype[job.runtype], job.scfcode)

# if runtype == "properties":
#   status = os.system("queue SCF_CamCASP {job} -q {args.queue} -w {d}"\
#              " -M {args.memory} --nproc {args.nproc} --scratch {job.work}"\
#              " --scfcode {scfcode}".format(**locals()))
# else:

sys.stdout.flush()
rc = 0
# print "Calling execute"
execute(job, verbosity, rc)

exit(rc)
